package gr.uoc.radio.fosscomm.loaders;

import android.content.Context;
import android.database.Cursor;
import gr.uoc.radio.fosscomm.db.DatabaseManager;
import gr.uoc.radio.fosscomm.model.Day;
import gr.uoc.radio.fosscomm.model.Track;

public class TrackScheduleLoader extends SimpleCursorLoader {

	private final Day day;
	private final Track track;

	public TrackScheduleLoader(Context context, Day day, Track track) {
		super(context);
		this.day = day;
		this.track = track;
	}

	@Override
	protected Cursor getCursor() {
		return DatabaseManager.getInstance().getEvents(day, track);
	}
}
