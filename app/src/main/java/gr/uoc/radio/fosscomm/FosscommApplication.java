package gr.uoc.radio.fosscomm;

import android.app.Application;
import android.support.v7.preference.PreferenceManager;

import gr.uoc.radio.fosscomm.alarms.FosscommAlarmManager;
import gr.uoc.radio.fosscomm.db.DatabaseManager;

public class FosscommApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		DatabaseManager.init(this);
		// Initialize settings
		PreferenceManager.setDefaultValues(this, R.xml.settings, false);
		// Alarms (requires settings)
		FosscommAlarmManager.init(this);
	}
}
