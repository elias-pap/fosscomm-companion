# FOSSCOMM Companion

Advanced native Android schedule browser application for the [FOSSCOMM 2018](https://fosscomm2018.gr/) conference in Heraklion, Greece.  
This app is a fork of <a href="https://github.com/cbeyls/fosdem-companion-android">FOSDEM Companion</a>, created by Christophe Beyls.

<a href="https://play.google.com/store/apps/details?id=gr.uoc.radio.fosscomm" target="_blank">
  <img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" height="80"/>
</a>

## How to build

All dependencies are defined in ```app/build.gradle```. Import the project in Android Studio or use Gradle in command line:

```
./gradlew assembleRelease
```

The result apk file will be placed in ```app/build/outputs/apk/```.

## License

[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Used libraries

* [Android Support Library](http://developer.android.com/tools/support-library/) by The Android Open Source Project
* [ViewPagerIndicator](http://viewpagerindicator.com/) by Jake Wharton
* [PhotoView](https://github.com/chrisbanes/PhotoView) by Chris Banes

## Contributors

* Christophe Beyls
* Elias Papavasileiou
